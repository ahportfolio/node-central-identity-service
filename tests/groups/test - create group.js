/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Fs = require('fs');
  const Assert = require('assert');

  const EndpointFactory = require('node-http-endpoint-tester-factory');
  const FixtureFactory = require('node-fixture-factory');
  const Chance = require('chance').Chance();

  const fixtures = new FixtureFactory({
    client: 'postgresql',
    connection: {
      user: process.env.APP_DB_USER,
      password: process.env.APP_DB_PASSWORD,
      host: process.env.APP_DB_HOST,
      port: process.env.APP_DB_PORT,
      database: process.env.APP_DB_NAME
    }
  });

  const endpointFactory = new EndpointFactory();

  const FIXTURE_TABLE_IDENTITIES = 'identity_manager_identities';

  const API_BLUEPRINT_RESOURCE_ROOT = __dirname + '/../../resources/api-blueprint';

  function _loadBlueprintResource(resourcePath) {
    return String(Fs.readFileSync(API_BLUEPRINT_RESOURCE_ROOT + resourcePath));
  }

  describe('bootstrap', function () {
    before(function (done) {
      fixtures.insert(FIXTURE_TABLE_IDENTITIES, new Array(2).fill(null).map(_=> {
        return {
          identifier: Chance.word(),
          realm: 'test realm',
          roles: '[]',
          auth_keys: '[]',
          access_keys: '[]',
          attributes: '{}',
          dt_created: Date.now()
        }
      }), done);
    });

    before(function() {
      endpointFactory.addDataStructureBlueprintSync(_loadBlueprintResource('/data-structures/identity-group.apib'));
    });

    it('should successfully generate tests', function () {
      let endpointCount = 0;
      for (let endpoint of endpointFactory.buildFromBlueprintSync(_loadBlueprintResource('/groups/group-collection/create-group.apib'))) {

        endpoint.server = process.env.TESTS_APP_HOST || require('../../src/_server');

        endpoint.overrideComponent('body', 'tags', (value) => {
          value.items.minLength = 1;
          value.items.maxLength = 55;
          return value;
        });
        
        endpoint.overrideComponent('body', 'members', (value) => {
          value.enum = [fixtures.retrieve(FIXTURE_TABLE_IDENTITIES, 'id')];
          value.minItems = 1;
          value.items.minimum = 1;
          return value;
        });
        
        endpoint.positiveAssert((req, res) => {
          Assert.deepEqual(res.body.tags.sort(), req.body.tags.sort());
          Assert.deepEqual(res.body.members.sort(), req.body.members.sort());
          Assert.deepEqual(res.body.attributes, req.body.attributes || {});
        });

        endpoint.negativeAssert((_, res, tags) => {
          Assert.equal(res.statusCode, 422);
          Assert.equal(res.body.code, 'ParameterValidationError');

          let detectedMissingParameters = new Set(res.body.data.errors.filter(e=>e.message === 'is required').map(e=>e.field));
          let detectedInvalidTypeParameters = new Set(res.body.data.errors.filter(e=>e.message === 'is the wrong type').map(e=>e.field.substr(5)));
          let detectedArbitraryErrorsArray = res.body.data.errors.filter(e=>e.message !== 'is required' && e.message !== 'is the wrong type').map(e=>e.field.substr(5));

          Assert.ok(res.body.data.errors.length === (detectedArbitraryErrorsArray.length + detectedMissingParameters.size + detectedInvalidTypeParameters.size));
          Assert.ok(tags.has('MissingParameters') && Array.from(tags).some(t=>t.startsWith('mp:') && !tags.has('o' + t)) ? detectedMissingParameters.size > 0 : detectedMissingParameters.size === 0);
          Assert.ok(tags.has('InvalidParameterType') && Array.from(tags).some(t=>t.startsWith('ip:') && !tags.has('mp:' + t.substr(3))) ? detectedInvalidTypeParameters.size > 0 : detectedInvalidTypeParameters.size == 0);
          Assert.ok(tags.has('InvalidParameterValue') && Array.from(tags).some(t=>t.startsWith('ivp:') && !tags.has('mp:' + t.substr(4))) ? detectedArbitraryErrorsArray.length > 0 : detectedArbitraryErrorsArray.length == 0);

          Assert.ok(Array.from(tags).filter(t=>t.startsWith('mp:') && !tags.has('o' + t)).map(t=>t.slice(t.lastIndexOf(':') + 1)).every(p=>detectedMissingParameters.has(p)));
          Assert.ok(Array.from(tags).filter(t=>t.startsWith('ip:') && !tags.has('mp:' + t.substr(3))).map(t=>t.slice(t.lastIndexOf(':') + 1)).every(p=>detectedInvalidTypeParameters.has(p)));
          Assert.ok(Array.from(tags).filter(t=>t.startsWith('ivp:') && !tags.has('mp:' + t.substr(4))).map(t=>t.slice(t.lastIndexOf(':') + 1)).every(p=>detectedArbitraryErrorsArray.some(f=>f === p || f.substr(0, f.lastIndexOf('.')) === p)));

          Assert.ok(Array.from(detectedMissingParameters).every(p=>['body', 'headers', 'params', 'query'].some(l=>tags.has('mp:' + l + ':' + p))));
          Assert.ok(Array.from(detectedInvalidTypeParameters).every(p=>['body', 'headers', 'params', 'query'].some(l=>tags.has('ip:' + l + ':' + p))));
          Assert.ok(detectedArbitraryErrorsArray.every(p=>['body', 'headers', 'params', 'query'].some(l=>tags.has('ivp:' + l + ':' + p) || tags.has('ivp:' + l + ':' + p.substr(0, p.lastIndexOf('.'))))));
        });

        describe((++endpointCount) + ' - create identity group', function () {
          let count = 0;
          for (let defn of endpoint.endpointTestIterator()) {
            it((++count) + ' - ' + defn.description, defn.execute)
          }

          after((done) => fixtures.removeAll(done));
        });
      }
    });
  });
}());