# central-identity-service

This application manages pure identity models and logical groups of these models.
In addition to group memberships, identities can have multiple access keys, each with an associated level,
to model a layered approach to authorization, where access control algorithms that operate at a
higher levels cannot be performed unless the lower levels authorizations have been completed.
This software was built using NodeJS to expose an HTTP/JSON interface as the primary means of data manipulation.

## Configuration

| Type |  Key  | Comments |
| ---- | :---: | -------- |
| ENV  | NODE_PORT | Port number to bind the interface |
| ENV  | APP_KEY_PATH | The path to the file that contains the private key to use for this app |
| ENV  | AUTH_BCRYPT_ROUNDS | The number of rounds to use with the bcrypt algorithm to create secure hashes (default 10) |
| ENV  | AUTH_MAX_ACCESS_KEYS | The maximum number of access keys to distribute for each identity (default 5) |
| ENV  | APP_NAME | A recognisable name to tag any output from the app |
| ENV  | APP_DB_USER | The name of user to authenticate with the postgres dbms  |
| ENV  | APP_DB_PASSWORD | The password to authenticate with the postgres dbms |
| ENV  | APP_DB_HOST | The host address of the postgres dbms |
| ENV  | APP_DB_PORT | The host port of the postgres dbms |
| ENV  | APP_DB_NAME | The name of the database to use |

## Identity Entity

| Field          | Data Type  | Comments |
| -------------- | :--------: | -------- |
| id             | INTEGER    | Primary key |
| realm          | TEXT       | A logical realm for this identity |
| identifier     | TEXT       | A stable locally unique identifier for this identity |
| roles          | ARRAY      | An array of roles assigned to this identity |
| auth_keys      | ARRAY      | An array of authentication keys associated with this identity |
| access_keys    | ARRAY      | An array of access tokens associated with this identity |
| attributes     | OBJECT     | An arbitrary JSON object of attributes associated with this identity |
| dt_created     | BIGINTEGER | Epoch timestamp representing the datetime when this identity was created |
| dt_updated     | BIGINTEGER / NULL | Epoch timestamp representing the datetime when this identity was last updated |

## Identity Group Entity

| Field            | Data Type  | Comments |
| ---------------- | :--------: | -------- |
| id               | INTEGER    | Primary key |
| tags             | ARRAY      | A collection of arbitrary tags to associate with this group |
| members          | ARRAY      | A collection of the primary keys for all the identities that are a part of this group |
| attributes       | OBJECT     | A collection of key value pairs associated with this group |
| dt_created       | BIGINTEGER | Epoch timestamp representing the datetime when this group was created |
| dt_updated       | BIGINTEGER / NULL | Epoch timestamp representing the datetime when this group was last updated |


## Operations

### Create identity

`POST /identities`

|  Parameter     | Location  | Data Type  | Comments |
| -------------- | :-------: | :--------: | -------- |
| realm          | BODY      | TEXT       | The realm for this identity |
| identifier     | BODY      | TEXT       | The stable unique identifier for this identity |
| [roles]        | BODY      | ARRAY      | The array of roles assigned to this identity |
| [authKeys]     | BODY      | ARRAY      | The array of authentication keys associated with this identity |
| [attributes]   | BODY      | OBJECT     | The arbitrary JSON object of attributes associated with this identity |

### Get identities

`GET /identities`

|  Parameter     |  Location | Data Type  | Comments |
| -------------- | :-------: | :--------: | -------- |
| offset         | QUERY     | INTEGER    | The number of records to skip |
| limit          | QUERY     | INTEGER    | The maximum number of records to return |
| [realm]        | QUERY     | TEXT       | The realm for the identity you want to retrieve |
| [identifier]   | QUERY     | TEXT       | The identifier for the identity you want to retrieve |
| [roles]        | QUERY     | ARRAY      | The roles that should be associated with the identities that you want to retrieve |
| [ids]          | QUERY     | ARRAY      | The collection of ids of the specific identities to retrieve |

### Update identity

`PUT /identities/{id}`

|  Parameter     | Location  | Data Type  | Comments |
| -------------- | :-------: | :--------: | -------- |
| [realm]        | BODY      | TEXT       | The realm for this identity |
| [identifier]   | BODY      | TEXT       | The unique identifier to assign to this identity |
| [roles]        | BODY      | ARRAY      | The array of roles to assign to this identity |
| [attributes]   | BODY      | OBJECT     | The attributes to associate with this identity |

### Update identity authentication key

`POST /identities/{id}/auth_keys/{level}`

|  Parameter         | Location  | Data Type  | Comments |
| ------------------ | :-------: | :--------: | -------- |
| authKey            | BODY      | TEXT       | The authentication key to assign to this identity at the specified level |

### Remove identity authentication key

`DELETE /identities/{id}/auth_keys/{level}`

_No parameters_

### Authenticate identity

`POST /identities/authentication`

|  Parameter         | Location  | Data Type  | Comments |
| ------------------ | :-------: | :--------: | -------- |
| realm              | BODY      | TEXT       | The realm of the identity that is being authenticated |
| identifier         | BODY      | TEXT       | The identifier of the identity that is being authenticated |
| authKey            | BODY      | TEXT       | The auth key to use to attempt the authentication |
| [level]            | BODY      | INTEGER    | The level of the authentication key being used to attempt the authentication |
| [clientId]         | BODY      | TEXT       | The id of the client that is attempting the authentication |

### Remove identity access key

`DELETE /identities/{id}/access_keys/{level}`

|  Parameter         | Location  | Data Type     | Comments |
| ------------------ | :-------: | :-----------: | -------- |
| clientId           | QUERY     | TEXT          | The id of the client whose access key you want to remove |

### Sign identity document

`POST /identities/{id}/signatures/{level}`

|  Parameter         | Location  | Data Type     | Comments |
| ------------------ | :-------: | :-----------: | -------- |
| clientId           | BODY      | TEXT          | The id of the client you want to sign the document for |
| document           | BODY      | TEXT / OBJECT | The document to use to generate the signature |

### Verify identity signature

`POST /identities/{id}/signatures/{level}/verification`

|  Parameter         | Location  | Data Type     | Comments |
| ------------------ | :-------: | :-----------: | -------- |
| ttl                | BODY      | INTEGER       | The maximum number of elapsed milliseconds to allow since the document was signed |
| signature          | BODY      | TEXT          | The signature to check |
| document           | BODY      | TEXT / OBJECT | The document to use to check the signature |
| clientId           | BODY      | TEXT          | The id of the client you want to verify the signature for |

### Mask identity document

`POST /identities/{id}/masks/{level}`

|  Parameter         | Location  | Data Type     | Comments |
| ------------------ | :-------: | :-----------: | -------- |
| document           | BODY      | TEXT / OBJECT | The document to use to create the mask |
| clientId           | BODY      | TEXT          | The id of the client you want to mask the document for |

### Unmask identity document

`POST /identities/{id}/masks/{level}/value`

|  Parameter     | Location  | Data Type     | Comments |
| -------------- | :-------: | :-----------: | -------- |
| maskedDocument | BODY      | TEXT          | The mask to use to build the unmasked value |
| clientId       | BODY      | TEXT          | The id of the client you want to unmask the document for |

### Delete identities

`DELETE /identities`

|  Parameter    | Location  | Data Type     | Comments |
| --------------| :-------: | :-----------: | -------- |
| [realm]       | QUERY     | TEXT          | The realm associated with the identities you want to remove |
| [identifier]  | QUERY     | TEXT          | The identifier associated with the identity you want to remove |
| [ids]         | QUERY     | ARRAY         | The collection of ids of the specific identities to remove |

### Create identity group

`POST /groups`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| tags             | BODY      | ARRAY      | The collection of arbitrary tags to associate with this group |
| members          | BODY      | ARRAY      | The collection of the primary keys for all the identities that are a part of this group |
| [attributes]     | BODY      | OBJECT     | The collection of key value pairs to associate with this group |

### Get identity groups

`GET /groups`

|  Parameter     |  Location | Data Type  | Comments |
| -------------- | :-------: | :--------: | -------- |
| offset         | QUERY     | INTEGER    | The number of records to skip |
| limit          | QUERY     | INTEGER    | The maximum number of records to return |
| [tags]         | QUERY     | ARRAY      | The collection of tags that must be associated with the groups that you want to retrieve |
| [members]      | QUERY     | ARRAY      | The collection of primary keys for the identifiers that must be a part of the groups that you want to retrieve |
| [ids]          | QUERY     | ARRAY      | The collection of ids of the specific groups that you want to retrieve |

### Update identity group

`PUT /groups/{id}`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| [tags]           | BODY      | ARRAY      | The collection of arbitrary tags to associate with this group |
| [attributes]     | BODY      | OBJECT     | The arbitrary JSON object of attributes associated with this group |

### Add identities to group

`POST /groups/{id}/members`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| members          | BODY      | ARRAY      | The collection of primary keys of the members that you want to add to this group |

### Remove identities from group

`DELETE /groups/{id}/members`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| members          | QUERY     | ARRAY      | The collection of primary keys of the members that you want to remove from this group |

### Remove identity groups

`DELETE /groups`

|  Parameter       | Location  | Data Type  | Comments |
| ---------------- | :-------: | :--------: | -------- |
| ids              | QUERY     | ARRAY      | The collection of ids of the groups that you want to remove |

