/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const EntityRepository = require('node-dbal-pg');

  var INSTANCE = null;

  class Repository extends EntityRepository {
    constructor() {
      super({
        username: process.env.APP_DB_USER,
        password: process.env.APP_DB_PASSWORD,
        host: process.env.APP_DB_HOST,
        port: process.env.APP_DB_PORT,
        database: process.env.APP_DB_NAME
      }, {
        table: 'identity_manager_groups',
        pk: 'id',
        converters: {
          members: EntityRepository.converters.JSON_INT_CAST_STRING_ARRAY,
          tags: EntityRepository.converters.JSON_IN,
          attributes: EntityRepository.converters.JSON_IN
        }
      })
    }

    static get sharedInstance() {
      if (INSTANCE === null) {
        INSTANCE = new Repository();

        if (process.env.APP_DB_DEBUG) {
          INSTANCE.client.enableDebugging();
        }
      }

      return INSTANCE;
    }
  }

  module.exports = Repository;
}());