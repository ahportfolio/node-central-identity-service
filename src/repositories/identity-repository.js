/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const EntityRepository = require('node-dbal-pg');

  var INSTANCE = null;

  class Repository extends EntityRepository {
    constructor() {
      super({
        username: process.env.APP_DB_USER,
        password: process.env.APP_DB_PASSWORD,
        host: process.env.APP_DB_HOST,
        port: process.env.APP_DB_PORT,
        database: process.env.APP_DB_NAME
      }, {
        table: 'identity_manager_identities',
        pk: 'id',
        converters: {
          roles: EntityRepository.converters.JSON_IN,
          auth_keys: EntityRepository.converters.JSON_IN,
          access_keys: EntityRepository.converters.JSON_IN,
          attributes: EntityRepository.converters.JSON_IN
        }
      })
    }

    static get sharedInstance() {
      if (INSTANCE === null) {
        INSTANCE = new Repository();

        if (process.env.APP_DB_DEBUG) {
          INSTANCE.client.enableDebugging();
        }
      }

      return INSTANCE;
    }
  }

  module.exports = Repository;
}());