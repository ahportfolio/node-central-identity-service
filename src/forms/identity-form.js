/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const DataForm = require('js-data-form');
  const Crypto = require('node-crypto-util');

  class EntityForm extends DataForm {
    constructor() {
      super({
        fields: {
          offset: {
            type: 'integer',
            minimum: 0
          },
          limit: {
            type: 'integer',
            minimum: 1
          },
          id: {
            type: 'integer',
            minimum: 1
          },
          _truncate: {},
          ids: {
            type: 'array',
            minItems: 1,
            items: {
              type: 'integer',
              minimum: 1
            }
          },
          realm: {
            type: 'string',
            minLength: 1,
            maxLength: 55
          },
          ttl: {
            type: 'integer'
          },
          signature: {
            type: 'string',
            minLength: 1,
            maxLength: 255
          },
          document: {
            type: 'string',
            minLength: 1,
            maxLength: 2048
          },
          masked_document: {
            type: 'string',
            minLength: 1,
            maxLength: 4096
          },
          identifier: {
            type: 'string',
            minLength: 1,
            maxLength: 55
          },
          roles: {
            type: 'array',
            items: {
              type: 'string',
              maxLength: 5
            }
          },
          auth_keys: {
            type: 'array',
            items: {
              type: 'string',
              minLength: 1,
              maxLength: 250
            }
          },
          auth_key: {
            type: 'string',
            minLength: 1,
            maxLength: 250
          },
          access_keys: {
            type: 'array',
            maxItems: 0
          },
          attributes: {
            type: 'object'
          },
          dt_created: {
            type: 'integer',
            minimum: 1
          },
          dt_updated: {
            type: 'integer',
            minimum: 1
          }
        }
      });
    }

    toSerializable(data) {
      return Object.assign(...[
        'id', 'realm', 'identifier', 'roles', 'attributes', 'dt_created', 'dt_updated'
      ].map(k=>({[k]: data[k]})), {
        authKeys: data.auth_keys.map(({key, level, dt_used, dt_created, dt_updated}) => {
          return {
            key: Crypto.Digests.md5(key),
            level,
            dt_used, dt_created, dt_updated
          }
        })
      }, {
        accessKeys: Object.assign({}, ...Object.keys(data.access_keys).map(clientId => {
          let accessKeys = data.access_keys[clientId].map(({key, level, clientId, dt_used, dt_created, dt_updated}) => {
            return {
              key: Crypto.Digests.md5(key),
              level, clientId,
              dt_used, dt_created, dt_updated
            }
          });
          return {[clientId]: accessKeys};
        }))
      });
    }
  }

  module.exports = EntityForm;
}());