/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const DataForm = require('js-data-form');

  class EntityForm extends DataForm {
    constructor() {
      super({
        fields: {
          offset: {
            type: 'integer',
            minimum: 0
          },
          limit: {
            type: 'integer',
            minimum: 1
          },
          id: {
            type: 'integer',
            minimum: 1
          },
          _truncate: {},
          ids: {
            type: 'array',
            minItems: 1,
            items: {
              type: 'integer',
              minimum: 1
            }
          },
          tags: {
            type: 'array',
            items: {
              type: 'string',
              minLength: 1,
              maxLength: 55
            }
          },
          members: {
            type: 'array',
            minItems: 1,
            items: {
              type: 'integer',
              minimum: 1
            }
          },
          attributes: {
            type: 'object'
          },
          dt_created: {
            type: 'integer',
            minimum: 1
          },
          dt_updated: {
            type: 'integer',
            minimum: 1
          }
        }
      });
    }

    toSerializable(data) {
      return Object.assign(...[
        'id', 'tags', 'members', 'attributes', 'dt_created', 'dt_updated'
      ].map(k=>({[k]: data[k]})));
    }
  }

  module.exports = EntityForm;
}());