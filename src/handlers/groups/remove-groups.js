/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const IdentityGroupRepository = require('../../repositories/identity-group-repository');
  const IdentityGroupForm = require('../../forms/identity-group-form');

  const identityGroupRepository = IdentityGroupRepository.sharedInstance;
  const identityGroupForm = new IdentityGroupForm();

  module.exports = [
    function extractParameters(req, _, next) {
      let query = req.query || {};
      
      req.context = {
        params: Util.omitBy({
          ids: Array.isArray(query.ids) ? query.ids.map(e=>isNaN(parseInt(e)) ? e : parseInt(e)) : query.ids,
          tags: query.tags,
          members: Array.isArray(query.members) ? query.members.map(e=>isNaN(parseInt(e)) ? e : parseInt(e)) : query.members,

          _truncate: query._truncate
        }, Util.isUndefined)
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityGroupForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      identityGroupRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function removeRecords({context}, _, next) {
      let query = Util.pick(context.params, []);
      if (context.params.ids) {
        query.id = {$in: context.params.ids};
      }

      if (context.params.members) {
        query.members = {$all: context.params.members};
      }

      if (context.params.tags) {
        query.tags = {$all: context.params.tags};
      }

      if (Object.keys(query).length === 0 && context.params._truncate !== 'YES') {
        return next(new Errors.InvalidRequestError({
          error: 'At least one criteria must be specified'
        }));
      }

      identityGroupRepository.remove(query, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.removedRecords = records;
        next();
      });
    },

    function commitTransaction({context}, _, next) {
      identityGroupRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json({
        totalRecords: context.removedRecords.length,
        records: context.removedRecords.map(e=>identityGroupForm.toSerializable(e)),

        ids: context.params.ids || null,
        tags: context.params.tags || null,
        members: context.params.members || null,

        _truncate: context.params._truncate
      });

      next();
    }];
}());