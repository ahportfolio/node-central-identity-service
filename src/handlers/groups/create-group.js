/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Errors = require('../../errors/api-errors');

  const IdentityRepository = require('../../repositories/identity-repository');

  const IdentityGroupRepository = require('../../repositories/identity-group-repository');
  const IdentityGroupForm = require('../../forms/identity-group-form');

  const identityRepository = IdentityRepository.sharedInstance;

  const identityGroupRepository = IdentityGroupRepository.sharedInstance;
  const identityGroupForm = new IdentityGroupForm();

  module.exports = [
    function extractParameters(req, _, next) {
      let body = req.body || {};

      req.context = {
        params: {
          tags: body.tags,
          members: Array.isArray(body.members) ? body.members.map(e=>isNaN(parseInt(e)) ? e : parseInt(e)) : body.members,
          attributes: body.attributes === undefined ? {} : body.attributes,
          dt_created: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityGroupForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      identityGroupRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function checkMemberReferences({context}, _, next) {
      context.params.members = Array.from(new Set(context.params.members)).sort();

      identityRepository.find({
        id: {$in: context.params.members}
      }, undefined, undefined, undefined, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        let foundReferences = new Set(records.map(e=>e.id));
        let invalidMemberReferences = new Set(context.params.members.filter(e=>!foundReferences.has(e)));

        if (invalidMemberReferences.size !== 0) {
          return next(new Errors.InvalidRequestError({
            invalidMembers: Array.from(invalidMemberReferences),
            error: 'Some of the members do not match any identity'
          }));
        }

        next();
      });
    },

    function createRecord({context}, _, next) {
      identityGroupRepository.insert(context.params, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.createdRecord = records[0];

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      identityGroupRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(identityGroupForm.toSerializable(context.createdRecord));

      next();
    }];
}());