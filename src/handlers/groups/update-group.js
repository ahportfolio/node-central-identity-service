/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const IdentityRepository = require('../../repositories/identity-repository');
  const IdentityForm = require('../../forms/identity-form');

  const IdentityGroupRepository = require('../../repositories/identity-group-repository');
  const IdentityGroupForm = require('../../forms/identity-group-form');

  const identityRepository = IdentityRepository.sharedInstance;
  const identityForm = new IdentityForm();

  const identityGroupRepository = IdentityGroupRepository.sharedInstance;
  const identityGroupForm = new IdentityGroupForm();

  module.exports = [
    function extractParameters(req, _, next) {
      let params = req.params || {};
      let body = req.body || {};
      
      req.context = {
        params: Util.omitBy({
          id: isNaN(parseInt(params.id)) ? params.id : parseInt(params.id),
          tags: body.tags,
          attributes: body.attributes,
          dt_updated: Date.now()
        }, Util.isUndefined)
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityGroupForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      identityGroupRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function updateRecord({context}, _, next) {
      let updateValues = Util.omit(context.params, ['id']);

      identityGroupRepository.updateByPk(context.params.id, updateValues, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('identity group', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },


    function findAssociatedIdentityRecords({context}, _, next) {
      identityRepository.find({
        id: {
          $in: context.updatedRecord.members
        }
      }, undefined, undefined, undefined, context.txn, (err, identityRecords) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.foundIdentityRecords = identityRecords;

        next();
      });
    },

    function buildSerializableResponse({context}, _, next) {
      let mappedIdentityRecords = {};

      for (let identity of context.foundIdentityRecords) {
        mappedIdentityRecords[identity.id] = identity;
      }

      context.serializableRecord = {
        group: identityGroupForm.toSerializable(context.updatedRecord),
        identities: context.updatedRecord.members.map(e => mappedIdentityRecords[e] || null).map(e=>e && identityForm.toSerializable(e))
      };

      next();
    },

    function commitTransaction({context}, _, next) {
      identityGroupRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(context.serializableRecord);

      next();
    }];
}());