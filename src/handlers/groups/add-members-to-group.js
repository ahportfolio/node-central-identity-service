/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const IdentityRepository = require('../../repositories/identity-repository');
  const IdentityForm = require('../../forms/identity-form');

  const IdentityGroupRepository = require('../../repositories/identity-group-repository');
  const IdentityGroupForm = require('../../forms/identity-group-form');

  const identityRepository = IdentityRepository.sharedInstance;
  const identityForm = new IdentityForm();

  const identityGroupRepository = IdentityGroupRepository.sharedInstance;
  const identityGroupForm = new IdentityGroupForm();

  module.exports = [
    function extractParameters(req, _, next) {
      let params = req.params || {};
      let body = req.body || {};

      req.context = {
        params: {
          id: isNaN(parseInt(params.id)) ? params.id : parseInt(params.id),

          members: Array.isArray(body.members) ? body.members.map(e=>isNaN(parseInt(e)) ? e : parseInt(e)) : body.members,
          dt_updated: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityGroupForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      identityGroupRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function checkMemberReferences({context}, _, next) {
      identityRepository.find({
        id: {$in: context.params.members}
      }, undefined, undefined, undefined, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        let foundReferences = new Set(records.map(e=>e.id));
        let invalidMemberReferences = new Set(context.params.members.filter(e=>!foundReferences.has(e)));

        if (invalidMemberReferences.size !== 0) {
          return next(new Errors.InvalidRequestError({
            invalidMembers: Array.from(invalidMemberReferences),
            error: 'Some of the members do not match any identity'
          }));
        }

        next();
      });
    },

    function updateRecord({context}, _, next) {
      let updateValues = Util.pick(context.params, ['dt_updated']);

      updateValues.members = IdentityGroupRepository.queries.APPEND_TO_JSONB_ARRAY('members', context.params.members.map(e=>String(e)));

      identityGroupRepository.updateByPk(context.params.id, updateValues, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('identity group', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },

    function findAssociatedIdentityRecords({context}, _, next) {
      identityRepository.find({
        id: {
          $in: context.updatedRecord.members
        }
      }, undefined, undefined, undefined, context.txn, (err, identityRecords) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.foundIdentityRecords = identityRecords;

        next();
      });
    },

    function buildSerializableResponse({context}, _, next) {
      let mappedIdentityRecords = {};

      for (let identity of context.foundIdentityRecords) {
        mappedIdentityRecords[identity.id] = identity;
      }

      context.serializableRecord = {
        group: identityGroupForm.toSerializable(context.updatedRecord),
        identities: context.updatedRecord.members.map(e => mappedIdentityRecords[e] || null).map(e=>e && identityForm.toSerializable(e))
      };

      next();
    },

    function commitTransaction({context}, _, next) {
      identityGroupRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(context.serializableRecord);

      next();
    }];
}());