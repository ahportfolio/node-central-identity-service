/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Errors = require('../../errors/api-errors');

  const identityRepository = new (require('../../repositories/identity-repository'))();
  const identityGroupRepository = new (require('../../repositories/identity-group-repository'))();
  const identityForm = new (require('../../forms/identity-form'))();
  const identityGroupForm = new (require('../../forms/identity-group-form'))();

  module.exports = [
    function extractParameters(req, _, next) {
      let params = req.params || {};
      let query = req.query || {};

      req.context = {
        params: {
          id: isNaN(parseInt(params.id)) ? params.id : parseInt(params.id),
          level: isNaN(parseInt(params.level)) ? params.level : parseInt(params.level),
          client_id: query.clientId,
          dt_updated: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },
    
    function beginTransaction({context}, _, next) {
      identityRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function prefetchRecordToUpdate({context}, _, next) {
      identityRepository.findByPk(context.params.id, context.txn, (err, foundRecord) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecord === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.recordToBeUpdated = foundRecord;

        next();
      });
    },

    function updateRecord({context}, _, next) {
      let {recordToBeUpdated: identity, params: {client_id, level, dt_updated}} = context;

      if (!identity.access_keys[client_id]) {
        context.updatedRecord = identity;
        return next();
      }

      identity.access_keys[client_id].splice(level);

      let updateValues = {
        access_keys: identity.access_keys,
        dt_updated: dt_updated
      };

      identityRepository.updateByPk(context.params.id, updateValues, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },

    function findAssociatedIdentityGroups({context}, _, next) {
      identityGroupRepository.find({
        members: {
          $exists: context.updatedRecord.id
        }
      }, undefined, undefined, undefined, context.txn, (err, groupRecords) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.foundGroupRecords = groupRecords;

        next();
      });
    },

    function buildSerializableResponse({context}, _, next) {
      context.serializableRecord = {
        identity: identityForm.toSerializable(context.updatedRecord),
        groups: context.foundGroupRecords.map(identityGroupForm.toSerializable.bind(identityGroupForm))
      };

      next();
    },

    function commitTransaction({context}, _, next) {
      identityRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(identityForm.toSerializable(context.updatedRecord));

      next();
    }];
}());