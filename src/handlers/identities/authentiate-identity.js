/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Fs = require('fs');

  const Crypto = require('node-crypto-util');
  const BCrypt = require('bcrypt');
  const UUID = require('node-uuid');
  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const identityRepository = new (require('../../repositories/identity-repository'))();
  const identityGroupRepository = new (require('../../repositories/identity-group-repository'))();
  const identityForm = new (require('../../forms/identity-form'))();
  const identityGroupForm = new (require('../../forms/identity-group-form'))();

  const APP_PRIVATE_KEY = (process.env.APP_KEY_PATH && Fs.readFileSync(process.env.APP_KEY_PATH, {encoding: 'utf8'}));
  if (!APP_PRIVATE_KEY) {
    throw new Error("A valid 'APP_KEY_PATH' must be provided");
  }

  const MAX_ACCESS_KEYS = +(process.env.AUTH_MAX_ACCESS_KEYS || 5);

  module.exports = [
    function extractParameters(req, _, next) {
      let body = req.body || {};

      req.context = {
        params: Util.omitBy({
          realm: body.realm,
          identifier: body.identifier,
          auth_key: body.authKey,

          level: isNaN(parseInt(body.level)) ? 0 : parseInt(body.level),
          client_id: body.clientId,
          dt_updated: Date.now()
        }, Util.isUndefined)
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      identityRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function prefetchRecordToUpdate({context}, _, next) {
      identityRepository.find({
        realm: context.params.realm,
        identifier: context.params.identifier
      }, context.txn, (err, foundRecords) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecords.length === 0) {
          return next(new Errors.UnknownIdentifierError('identity', {
            realm: context.params.realm,
            identifier: context.params.identifier
          }));
        }

        context.recordToBeUpdated = foundRecords[0];

        next();
      });
    },

    function verifyAuthKey({context}, _, next) {
      let {recordToBeUpdated: identity, params: {client_id, level, auth_key}} = context;

      if (level >= identity.auth_keys.length) {
        return next(new Errors.InvalidRequestError({
          level: level,
          identity: identityForm.toSerializable(identity),
          error: 'This identity does not have an auth key at this level'
        }));
      }

      if (client_id && !identity.access_keys[client_id]) {
        return next(new Errors.InvalidRequestError({
          clientId: client_id,
          identity: identityForm.toSerializable(identity),
          error: 'This identity does not have a client with this id'
        }));
      }

      if (client_id && level > identity.access_keys[client_id].length) {
        return next(new Errors.InvalidRequestError({
          level: level,
          identity: identityForm.toSerializable(identity),
          error: 'This client has not yet fully authenticated all the levels below this level'
        }));
      }

      let storedAuthKey = identity.auth_keys[level];
      BCrypt.compare(storedAuthKey, auth_key, (err, isCorrect) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (!isCorrect) {
          return next(new Errors.InvalidAuthenticationCredentials({
            level,
            identity: identityForm.toSerializable(identity)
          }));
        }

        next();
      });
    },

    function createAccessKey({context}, _, next) {
      let {recordToBeUpdated: identity, params: {client_id: clientId, level, dt_updated}} = context;

      if (MAX_ACCESS_KEYS === 0) {
        context.createdAccessKeyObject = null;
        return next();
      }

      if (!clientId) {
        if (Object.keys(identity.access_keys).length >= MAX_ACCESS_KEYS) {
          let lruClientId;
          let lruClientTimestamp = Number.MAX_SAFE_INTEGER;
          for (let clientId of Object.keys(identity.access_keys)) {
            let clientLastUsed = 0;

            for (let keyObject of identity.access_keys[clientId]) {
              if (keyObject.dt_used > clientLastUsed) {
                clientLastUsed = keyObject.dt_used;
              }
            }

            if (clientLastUsed < lruClientTimestamp) {
              lruClientTimestamp = clientLastUsed;
              lruClientId = clientId;
            }
          }

          context.ejectedClientId = lruClientId;
        }

        let idTestCount = 0;
        do {
          clientId = UUID.v4();
          idTestCount++;
        } while (identity.access_keys[clientId] && idTestCount < 10);

        if (!clientId) {
          return next(new Errors.UnexpectedError(new Error('A unique client id could not be generated after far more tries that should be necessary. Please try again later')));
        }
      }

      context.createdAccessKeyObject = {
        level,
        clientId: clientId,
        accessKey: UUID.v4(),
        dt_used: dt_updated,
        dt_created: dt_updated
      };

      next();
    },

    function updateRecord({context}, _, next) {
      let {recordToBeUpdated: identity, params: {level, dt_updated}} = context;

      identity.auth_keys[level].dt_used = dt_updated;

      if (context.ejectedClientId) {
        delete identity.access_keys[context.ejectedClientId];
      }

      if (context.createdAccessKeyObject) {
        let clientId = context.createdAccessKeyObject.clientId;
        identity.access_keys[clientId] = identity.access_keys[clientId] || [];
        identity.access_keys[clientId].splice(level);
        identity.access_keys[clientId].push(Object.assign({}, context.createdAccessKeyObject, {
          key: Crypto.AES.encrypt(context.createdAccessKeyObject.key, APP_PRIVATE_KEY, 'base64', 'aes-256-cbc')
        }));
      }

      let updateValues = {
        auth_keys: identity.auth_keys,
        access_keys: identity.access_keys,
        dt_updated: dt_updated
      };

      identityRepository.updateByPk(context.params.id, updateValues, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },

    function findAssociatedIdentityGroups({context}, _, next) {
      identityGroupRepository.find({
        members: {
          $exists: context.updatedRecord.id
        }
      }, undefined, undefined, undefined, context.txn, (err, groupRecords) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.foundGroupRecords = groupRecords;

        next();
      });
    },

    function buildSerializableResponse({context}, _, next) {
      context.serializableRecord = {
        identity: identityForm.toSerializable(context.updatedRecord),
        groups: context.foundGroupRecords.map(identityGroupForm.toSerializable.bind(identityGroupForm)),
        accessKey: context.createdAccessKeyObject
      };

      next();
    },

    function commitTransaction({context}, _, next) {
      identityRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(context.serializableRecord);

      next();
    }];
}());