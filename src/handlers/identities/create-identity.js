/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const BCrypt = require('bcrypt');
  const Errors = require('../../errors/api-errors');

  const identityRepository = require('../../repositories/identity-repository').sharedInstance;
  const identityForm = new (require('../../forms/identity-form'))();

  const BCRYPT_ROUNDS = +(process.env.AUTH_BCRYPT_ROUNDS || 10);

  function _forEachAsync(arr, eachCb, doneCb) {
    let accumulator = [].fill(null, 0, arr.length);
    let isDone = false;
    let doneCount = 0;
    if (!arr || arr.length === 0) {
      return doneCb(null, accumulator);
    }

    for (let i = 0; i < arr.length; i++) {
      eachCb(arr[i], i, (err, result) => {
        accumulator[i] = result;
        doneCount++;

        if (err) {
          isDone = true;
          return doneCb(err, accumulator);
        }

        if (doneCount === arr.length) {
          return doneCb(null, accumulator);
        }
      });
    }
  }

  module.exports = [
    function extractParameters(req, _, next) {
      let body = req.body || {};

      req.context = {
        params: {
          realm: body.realm,
          identifier: body.identifier,
          roles: body.roles === undefined ? [] : body.roles,
          auth_keys: body.authKeys === undefined ? [] : body.authKeys,
          access_keys: [],
          attributes: body.attributes === undefined ? {} : body.attributes,
          dt_created: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function hashAuthKeys({context}, _, next) {
      _forEachAsync(context.params.auth_keys, (key, i, cb) => {
        BCrypt.hash(key, BCRYPT_ROUNDS, (err, hashedKey) => {
          cb(err, {
            level: i,
            key: hashedKey,
            dt_used: null,
            dt_created: context.params.dt_created,
            dt_updated: null
          });
        });
      }, (err, keyObjects) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.params.auth_keys = keyObjects;

        next();
      });
    },

    function beginTransaction({context}, _, next) {
      identityRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function checkDuplicateIdentifier({context}, _, next) {
      identityRepository.count({
        identifier: context.params.identifier,
        realm: context.params.realm
      }, context.txn, (err, recordCount) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (recordCount > 0) {
          return next(new Errors.DuplicateIdentifier('identity', {
            identifier: context.params.identifier,
            realm: context.params.realm
          }));
        }

        next();
      });
    },

    function createRecord({context}, _, next) {
      identityRepository.insert(context.params, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.createdRecord = records[0];

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      identityRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(identityForm.toSerializable(context.createdRecord));

      next();
    }];
}());