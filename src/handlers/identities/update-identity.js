/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const identityRepository = new (require('../../repositories/identity-repository'))();
  const identityForm = new (require('../../forms/identity-form'))();

  module.exports = [
    function extractParameters(req, _, next) {
      let params = req.params || {};
      let body = req.body || {};

      req.context = {
        params: Util.omitBy({
          id: isNaN(parseInt(params.id)) ? params.id : parseInt(params.id),
          realm: body.realm,
          identifier: body.identifier,
          roles: body.roles,
          attributes: body.attributes,
          dt_updated: Date.now()
        }, Util.isUndefined)
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      identityRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function prefetchRecordToUpdate({context}, _, next) {
      if (context.params.realm && context.params.identifier) {
        return next();
      }

      identityRepository.findByPk(context.params.id, context.txn, (err, foundRecord) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecord === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.recordToBeUpdated = foundRecord;

        next();
      });
    },

    function checkDuplicateIdentifier({context}, _, next) {
      if (context.params.realm === undefined && context.params.identifier === undefined) {
        return next();
      }

      let effectiveQuery = Object.assign({
        identifier: context.params.identifier || context.recordToBeUpdated.identifier,
        realm: context.params.realm || context.recordToBeUpdated.realm
      });

      identityRepository.count(Object.assign({
        id: {$neq: context.params.id}
      }, effectiveQuery), context.txn, (err, recordCount) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (recordCount > 0) {
          return next(new Errors.DuplicateIdentifier('identity',effectiveQuery));
        }

        next();
      });
    },

    function updateRecord({context}, _, next) {
      let updateValues = Util.omit(context.params, ['id']);

      identityRepository.updateByPk(context.params.id, updateValues, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },

    function commitTransaction({context}, _, next) {
      identityRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(identityForm.toSerializable(context.updatedRecord));

      next();
    }];
}());