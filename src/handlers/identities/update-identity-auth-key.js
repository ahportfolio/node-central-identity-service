/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const BCrypt = require('bcrypt');

  const Errors = require('../../errors/api-errors');

  const identityRepository = new (require('../../repositories/identity-repository'))();
  const identityGroupRepository = new (require('../../repositories/identity-group-repository'))();
  const identityForm = new (require('../../forms/identity-form'))();
  const identityGroupForm = new (require('../../forms/identity-group-form'))();

  const BCRYPT_ROUNDS = +(process.env.AUTH_BCRYPT_ROUNDS || 10);

  module.exports = [
    function extractParameters(req, _, next) {
      let params = req.params || {};
      let body = req.body || {};

      req.context = {
        params: {
          id: isNaN(parseInt(params.id)) ? params.id : parseInt(params.id),
          level: isNaN(parseInt(params.level)) ? params.level : parseInt(params.level),
          auth_key: body.authKey,
          dt_updated: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },
    
    function hashAuthKey({context}, _, next) {
      BCrypt.hash(context.auth_key, BCRYPT_ROUNDS, (err, hashedKey) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.auth_key = hashedKey;

        next();
      });
    },

    function beginTransaction({context}, _, next) {
      identityRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function prefetchRecordToUpdate({context}, _, next) {
      identityRepository.findByPk(context.params.id, context.txn, (err, foundRecord) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecord === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.recordToBeUpdated = foundRecord;

        next();
      });
    },

    function updateRecord({context}, _, next) {
      let {recordToBeUpdated: identity, params: {level, auth_key, dt_updated}} = context;

      if (level > identity.auth_keys.length) {
        return next(new Errors.InvalidRequestError({
          level: level,
          identity: identityForm.toSerializable(identity),
          error: 'An auth key cannot be assigned to this identitiy at this level'
        }));
      } else if (level === identity.auth_keys.length) {
        identity.auth_keys.push({
          level: level,
          key: auth_key,
          dt_used: null,
          dt_created: dt_updated,
          dt_updated: null
        });
      }

      identity.auth_keys[level].key = auth_key;
      identity.auth_keys[level].dt_updated = dt_updated;

      for(let clientId of Object.keys(identity.access_keys)) {
        identity.access_keys[clientId].splice(level);
      }
      
      let updateValues = {
        auth_keys: identity.auth_keys,
        access_keys: identity.access_keys,
        dt_updated: dt_updated
      };

      identityRepository.updateByPk(context.params.id, updateValues, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },

    function findAssociatedIdentityGroups({context}, _, next) {
      identityGroupRepository.find({
        members: {
          $exists: context.updatedRecord.id
        }
      }, undefined, undefined, undefined, context.txn, (err, groupRecords) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.foundGroupRecords = groupRecords;

        next();
      });
    },

    function buildSerializableResponse({context}, _, next) {
      context.serializableRecord = {
        identity: identityForm.toSerializable(context.updatedRecord),
        groups: context.foundGroupRecords.map(identityGroupForm.toSerializable.bind(identityGroupForm))
      };

      next();
    },

    function commitTransaction({context}, _, next) {
      identityRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(identityForm.toSerializable(context.updatedRecord));

      next();
    }];
}());