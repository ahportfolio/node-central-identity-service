/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Util = require('lodash');

  const Errors = require('../../errors/api-errors');

  const IdentityRepository = require('../../repositories/identity-repository');
  const IdentityForm = require('../../forms/identity-form');

  const IdentityGroupRepository = require('../../repositories/identity-group-repository');
  const IdentityGroupForm = require('../../forms/identity-group-form');

  const identityRepository = IdentityRepository.sharedInstance;
  const identityForm = new IdentityForm();

  const identityGroupRepository = IdentityGroupRepository.sharedInstance;
  const identityGroupForm = new IdentityGroupForm();

  module.exports = [
    function extractParameters(req, _, next) {
      let query = req.query || {};

      req.context = {
        params: Util.omitBy({
          ids: Array.isArray(query.ids) ? query.ids.map(e=>isNaN(parseInt(e)) ? e : parseInt(e)) : query.ids,
          identifier: query.identifier,
          realm: query.realm,
          _truncate: query._truncate
        }, Util.isUndefined)
      };

      next();
    },

    function validateParameters({context}, _, next) {
      if (context.params.identifier && !context.params.realm) {
        return next(new Errors.InvalidRequestError({
          identifier: context.params.identifier,
          realm: null,
          error: 'If an identifier is specified a realm must also be specified'
        }));
      }

      let {isValid, errors} = identityForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      identityRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function removeRecords({context}, _, next) {
      let query = Util.pick(context.params, ['realm', 'identifier']);
      if (context.params.ids) {
        query.id = {$in: context.params.ids};
      }

      if (Object.keys(query).length === 0 && context.params._truncate !== 'YES') {
        return next(new Errors.InvalidRequestError({
          error: 'At least one criteria must be specified'
        }));
      }

      identityRepository.find(query, undefined, undefined, undefined, context.txn, (err, records) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        let idsToRemove = records.map(record => record.id);

        identityRepository.remove({id: {$in: idsToRemove}}, context.txn, (err, records) => {
          next.ifError(err && new Errors.UnexpectedError(err));

          context.idsRemoved = idsToRemove;
          context.removedRecords = records;

          next();
        });
      });
    },

    function cleanUpGroups({context}, _, next) {
      if (context.removedRecords.length === 0) {
        context.updatedGroupRecords = context.removedGroupRecords = [];
        return next();
      }

      identityGroupRepository.update({
        members: IdentityGroupRepository.queries.OMIT_FROM_JSONB_ARRAY('members', context.idsRemoved.map(e=>String(e)))
      }, {members: {$any: context.idsRemoved}}, context.txn, (err, updatedGroupRecords) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        identityGroupRepository.remove({
          $subquery: {
            query: IdentityGroupRepository.queries.LENGTH_JSONB_ARRAY('members'),
            value: {
              $eq: 0
            }
          }
        }, context.txn, (err, removedGroupRecords) => {
          next.ifError(err && new Errors.UnexpectedError(err));

          let removedGroupIds = new Set(removedGroupRecords.map(e => e.id));
          
          context.updatedGroupRecords = updatedGroupRecords.filter(e=>!removedGroupIds.has(e.id));
          context.removedGroupRecords = removedGroupRecords;

          next();
        });
      });
    },

    function commitTransaction({context}, _, next) {
      identityRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json({
        totalRecords: context.removedRecords.length,
        records: context.removedRecords.map(identityForm.toSerializable.bind(identityForm)),
        updatedGroupRecords: context.updatedGroupRecords.map(identityGroupForm.toSerializable.bind(identityGroupForm)),
        removedGroupRecords: context.removedGroupRecords.map(identityGroupForm.toSerializable.bind(identityGroupForm)),

        ids: context.params.ids || null,
        identifier: context.params.identifier || null,
        realm: context.params.realm || null,

        _truncate: context.params._truncate
      });

      next();
    }];
}());