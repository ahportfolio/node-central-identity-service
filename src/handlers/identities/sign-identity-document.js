/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Fs = require('fs');

  const Crypto = require('node-crypto-util');

  const Errors = require('../../errors/api-errors');

  const identityRepository = new (require('../../repositories/identity-repository'))();
  const identityGroupRepository = new (require('../../repositories/identity-group-repository'))();
  const identityForm = new (require('../../forms/identity-form'))();
  const identityGroupForm = new (require('../../forms/identity-group-form'))();

  const APP_PRIVATE_KEY = (process.env.APP_KEY_PATH && Fs.readFileSync(process.env.APP_KEY_PATH, {encoding: 'utf8'}));
  if (!APP_PRIVATE_KEY) {
    throw new Error("A valid 'APP_KEY_PATH' must be provided");
  }

  module.exports = [
    function extractParameters(req, _, next) {
      let params = req.params || {};
      let body = req.body || {};

      req.context = {
        params: {
          id: isNaN(parseInt(params.id)) ? params.id : parseInt(params.id),
          level: isNaN(parseInt(params.level)) ? params.level : parseInt(params.level),

          client_id: body.clientId,
          document: body.document,
          dt_updated: Date.now()
        }
      };

      next();
    },

    function validateParameters({context}, _, next) {
      let {isValid, errors} = identityForm.validate(context.params, '*');

      if (!isValid) {
        return next(new Errors.ParameterValidationError(context.params, errors));
      }

      next();
    },

    function beginTransaction({context}, _, next) {
      identityRepository.beginTransaction((err, txn) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = txn;

        next();
      });
    },

    function prefetchRecordToUpdate({context}, _, next) {
      identityRepository.findByPk(context.params.id, context.txn, (err, foundRecord) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (foundRecord === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.recordToBeUpdated = foundRecord;

        next();
      });
    },

    function generateSignature({context}, _, next) {
      let {recordToBeUpdated: identity, params: {document, client_id, level, dt_updated}} = context;

      if (!identity.access_keys[client_id]) {
        return next(new Errors.UnknownIdentifierError('client', {
          clientId: client_id
        }));
      }

      if (level >= identity.access_keys[client_id].length) {
        return next(new Errors.InsufficientAuthorization({
          level: level
        }));
      }

      let accessKeys = identity.access_keys[client_id].slice(0, level + 1).map(key => {
        return Crypto.AES.decrypt(key, APP_PRIVATE_KEY, 'base64', 'aes-256-cbc');
      });

      let interimDocument = document;

      for (let accessKey of accessKeys) {
        interimDocument = Crypto.Digests.hmac(interimDocument, accessKey, 'base64', 'sha256');
      }
      context.generatedSignature = Crypto.Signature.signDocument(
          interimDocument, accessKeys[accessKeys.length - 1], dt_updated,
          'base64', 'sha256', 'identity_document_signature');

      next();
    },

    function updateRecord({context}, _, next) {
      let {recordToBeUpdated: identity, params: {client_id, level, dt_updated}} = context;

      identity.access_keys[client_id][level].dt_used = dt_updated;

      let updateValues = {
        access_keys: identity.access_keys,
        dt_updated: dt_updated
      };

      identityRepository.updateByPk(context.params.id, updateValues, context.txn, (err, record) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        if (record === null) {
          return next(new Errors.UnknownIdentifierError('identity', context.params.id));
        }

        context.updatedRecord = record;

        next();
      });
    },

    function findAssociatedIdentityGroups({context}, _, next) {
      identityGroupRepository.find({
        members: {
          $exists: context.updatedRecord.id
        }
      }, undefined, undefined, undefined, context.txn, (err, groupRecords) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.foundGroupRecords = groupRecords;

        next();
      });
    },

    function buildSerializableResponse({context}, _, next) {
      context.serializableRecord = {
        identity: identityForm.toSerializable(context.updatedRecord),
        groups: context.foundGroupRecords.map(identityGroupForm.toSerializable.bind(identityGroupForm)),
        signature: context.generatedSignature
      };

      next();
    },

    function commitTransaction({context}, _, next) {
      identityRepository.commitTransaction(context.txn, (err) => {
        next.ifError(err && new Errors.UnexpectedError(err));

        context.txn = undefined;

        next();
      });
    },

    function sendResponse({context}, res, next) {
      res.json(context.serializableRecord);

      next();
    }];
}());