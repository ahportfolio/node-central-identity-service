/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const PrettyStream = require('bunyan-prettystream');

  function _buildBunyanPrettyStdoutStream() {
    let prettyStdOut = new PrettyStream();
    prettyStdOut.pipe(process.stdout);

    return prettyStdOut;
  }

  /**
   *
   * @type {Server}
   */
  const server = require('./_server');
  const logger = server.log.child({stream: process.env.APP_LOG_PRETTY ? _buildBunyanPrettyStdoutStream() : process.stdout});

  server.listen(process.env.NODE_PORT, function () {
    //noinspection JSUnresolvedVariable
    logger.info('Application listening on: %s', server.url);
  });

  module.exports = server;
}());