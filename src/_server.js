/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  const Restify = require('restify');

  const PrettyStream = require('bunyan-prettystream');

  function _buildBunyanPrettyStdoutStream() {
    let prettyStdOut = new PrettyStream();
    prettyStdOut.pipe(process.stdout);

    return prettyStdOut;
  }

  const server = Restify.createServer({name: process.env.APP_NAME});
  //noinspection JSUnresolvedFunction
  const logger = server.log.child({
    stream: process.env.APP_LOG_PRETTY ? _buildBunyanPrettyStdoutStream() : process.stdout,
    serializers: {
      err: (err) => {
        if (err instanceof Restify.RestError) {
          return String(err);
        }
        return server.log.serializers.err(err);
      }
    }
  });

  server.use(Restify.acceptParser(server.acceptable));
  server.use(Restify.CORS());
  server.use(Restify.queryParser({mapParams: false}));
  server.use(Restify.bodyParser({mapParams: false}));
  server.use(Restify.requestLogger());
  server.use(Restify.gzipResponse());
  server.use(Restify.conditionalRequest());

  // register handlers
  server.post('/identities', require('./handlers/identities/create-identity'));
  server.get('/identities', require('./handlers/identities/get-identities'));
  server.put('/identities/:id', require('./handlers/identities/update-identity'));
  server.del('/identities', require('./handlers/identities/remove-identities'));
  server.post('/identities/:id/auth_keys/:level', require('./handlers/identities/update-identity-auth-key'));
  server.del('/identities/:id/auth_keys/:level', require('./handlers/identities/remove-identity-auth-key'));
  server.post('/identities/authentication', require('./handlers/identities/authentiate-identity'));
  server.del('/identities/:id/access_keys/:level', require('./handlers/identities/remove-identity-access-key'));
  server.post('/identities/:id/signatures/:level', require('./handlers/identities/sign-identity-document'));
  server.post('/identities/:id/signatures/:level/verification', require('./handlers/identities/verify-identity-document-signature'));
  server.post('/identities/:id/masks/:level', require('./handlers/identities/mask-identity-document'));
  server.post('/identities/:id/masks/:level/value', require('./handlers/identities/unmask-identity-document'));

  server.post('/groups', require('./handlers/groups/create-group'));
  server.get('/groups', require('./handlers/groups/get-groups'));
  server.put('/groups/:id', require('./handlers/groups/update-group'));
  server.del('/groups', require('./handlers/groups/remove-groups'));
  server.post('/groups/:id/members', require('./handlers/groups/add-members-to-group'));
  server.del('/groups/:id/members', require('./handlers/groups/remove-members-from-group'));

  if (!process.env.APP_LOG_DISABLE) {
    server.on('after', Restify.auditLogger({log: logger, body: true}));
  }

  server.on('after', (req, res, route, err) => {
    if (req && req.context && req.context.txn) {
      let txn = req.context.txn;
      txn.client.query('ROLLBACK', (err) => {
        txn.done(err);

        if (err) {
          req.log.error({err: err});
        }
      });
    }
  });

  server.on('uncaughtException', function (req, res, route, error) {
    req.log.error({err: error});

    try {
      res.send(new Restify.InternalError(error, error.message || 'Unexpected application error'));
    } catch (e) {
      req.log.error({err: e});
    }

    setTimeout(() => {
      server.close(() => {
        logger.info('Encountered an unhandled error so shutting down the server');
        process.exit(1);
      }, 2000);
    });
  });

  /**
   *
   * @type {Server}
   */
  module.exports = server;
}());