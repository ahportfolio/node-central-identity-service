/**
 * Created by Aston Hamilton
 */

(function () {
  'use strict';

  const restify = require('restify');

  class APIError extends restify.RestError {
    constructor(constructorOpt, message, data = {}, err) {
      super({
        statusCode: 422,
        message: err ? String(err) + ': ' + message : message,
        restCode: constructorOpt.name,
        constructorOpt: constructorOpt
      });

      //noinspection JSUnresolvedVariable
      this.body.data = data;
    }
  }

  class ParameterValidationError extends APIError {
    constructor(parameters, errors, err = undefined) {
      super(ParameterValidationError, 'The parameters are not valid', {
        parameters, errors
      }, err);
    }
  }

  class InvalidRequestError extends APIError {
    constructor(data, err = undefined) {
      super(InvalidRequestError, 'The request is not valid', data, err);
    }
  }

  class UnexpectedError extends APIError {
    constructor(err) {
      super(UnexpectedError, 'An unexpected error has occurred', undefined, err);
    }
  }

  class UnknownIdentifierError extends APIError {
    constructor(targetName, identifier, err = undefined) {
      super(UnknownIdentifierError, `No ${targetName} matched the identifier`, {
        identifier
      }, err);
    }
  }

  class DuplicateIdentifier extends APIError {
    constructor(targetName, identifier, err = undefined) {
      super(DuplicateIdentifier, `${
          ['a', 'e', 'i', 'o', 'u'].indexOf(targetName.charAt(0).toLocaleLowerCase()) > -1 ? 'An' : 'A'
          } ${targetName} with this identifier already exists`, {
        identifier
      }, err);
    }
  }

  class InvalidAuthenticationCredentials extends APIError {
    constructor(data = {}, err = undefined) {
      super(InvalidAuthenticationCredentials, 'The authentication credentials are not valid', data, err);
    }
  }

  class InsufficientAuthorization extends APIError {
    constructor(data = {}, err = undefined) {
      super(InsufficientAuthorization, 'This client has not been authenticated at all the necessary levels', data, err);
    }
  }

  module.exports = {
    APIError,
    UnknownIdentifierError,
    UnexpectedError,
    ParameterValidationError,
    DuplicateIdentifier,
    InvalidRequestError,
    InvalidAuthenticationCredentials,
    InsufficientAuthorization
  };
}());
