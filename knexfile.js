// Update with your config settings.

module.exports = {
  client: 'postgresql',
  connection: {
    user: process.env.APP_DB_USER,
    password: process.env.APP_DB_PASSWORD,
    host: process.env.APP_DB_HOST,
    port: process.env.APP_DB_PORT,
    database: process.env.APP_DB_NAME
  },
  migrations: {
    tableName: 'knex_migrations'
  }
};
