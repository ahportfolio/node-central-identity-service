/**
 * Created by Aston Hamilton
 */
(function () {
  'use strict';

  module.exports.up = function (knex, Promise) {
    return Promise.join(...[
        knex.schema.createTable('identity_manager_identities', function (table) {
          table.increments('id').notNullable();
          table.text('identifier').notNullable();
          table.text('realm').notNullable();
          table.jsonb('roles').notNullable();
          table.jsonb('auth_keys').notNullable();
          table.jsonb('access_keys').notNullable();
          table.jsonb('attributes').notNullable();
          table.bigInteger('dt_created').notNullable();
          table.bigInteger('dt_updated').nullable();

          table.index(['identifier', 'realm']);
          table.index(['roles'], 'identity_manager_identities_roles_index', 'gin');
        }),

        knex.schema.createTable('identity_manager_groups', function (table) {
          table.increments('id').notNullable();
          table.jsonb('members').notNullable();
          table.jsonb('attributes').notNullable();
          table.jsonb('tags').notNullable();
          table.bigInteger('dt_created').notNullable();
          table.bigInteger('dt_updated').nullable();

          table.index(['members'], 'identity_manager_groups_members_index', 'gin');
          table.index(['tags'], 'identity_manager_groups_tags_index', 'gin');
        })
    ]);
  };

  module.exports.down = function (knex, Promise) {
    return Promise.join(...[
        knex.schema.dropTable('identity_manager_identities'),
        knex.schema.dropTable('identity_manager_groups')
    ]);
  };

}());